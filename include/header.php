<?php 
$URL ='http://localhost/ms/';
$BASE_URL='http://localhost/ms/app/';
?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Marksheet</title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <style type="text/css">




        header{
          color: #2E86C1;
         
          
        }
/* Add a black background color to the top navigation */
        .topnav {
            background-color: rgb(248, 248, 248);
            overflow: hidden;

            
          }
          
          /* Style the links inside the navigation bar */
          .topnav a {
            float: left;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
          }
          
          /* Change the color of links on hover */
          .topnav a:hover {
            background-color: #ddd;
            color: black;
          }
          
          /* Add a color to the active/current link */
          .topnav a.active {
            background-color: rgb(161, 161, 161);
            color: white;
          } 
          /*body{
            padding-top: 30px;
          }*/
          table, th,td {
            background-color: #ffffff;
            border: 1px solid gray;
            width: 100%;
            padding: 12px 20px;
           /* margin: 5px 0;*/
            

          }
          table{
            border-collapse: collapse;
          }
          th{
            color: #696969;
            border: 1px solid gray;
          }
          input[type=text], input[type=number],input[type=submit]{
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
          }

  footer{
    /*background-color: #EAECEE ;*/
    color: #2E86C1;
    text-align: center;
  }

  h6{
    color: #2E86C1;
  }



</style>
</head>
<body>
  <div class="container">
    <header>
      <h3>Student Result Management System</h3>
      <p>Prasadi Academy</p>
    </header>

    <div class="topnav">
                <a class="active" href="<?php echo $URL;?>index.php">Home</a>
                <a href="<?php echo $BASE_URL;?>student/index.php">Student</a>
                <a href="<?php echo $BASE_URL;?>subject/index.php">Subject</a>
                <a href="<?php echo $BASE_URL;?>mark/index.php">Marks</a>
                <a href="<?php echo $BASE_URL;?>class/index.php">Class</a>
                <a href="<?php echo $BASE_URL;?>terminal/index.php">Terminal</a>
                <a href="<?php echo $BASE_URL;?>viewresult/index.php">View Result</a>
        </div>
   
  
       
    </body>
    </html>
