<?php

$base = '../../include/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{    
    $id = $crud->escape_string($_POST['id']);
    
    $studentid = $crud->escape_string($_POST['studentid']);
    $subjectid = $crud->escape_string($_POST['subjectid']);
    $marks = $crud->escape_string($_POST['marks']);
    
    
    $msg = $validation->check_empty($_POST, array('studentid','subjectid','marks'));
   
    
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    }  else {    
        //updating the table
        $result = $crud->execute("UPDATE studentmarks SET studentid='$studentid',subjectid='$subjectid',marks='$marks' WHERE id=$id");
        
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>