<?php
// including the database connection file
$base = '../../include/';
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//getting id from url
$id = $crud->escape_string($_GET['id']);
 
//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM students WHERE id=$id");
 
foreach ($result as $res) {
    $name = $res['name'];
    $age = $res['age'];
    $email = $res['email'];
    $gender = $res['gender'];
    $dateofbirth = $res['dateofbirth'];
   
    $classid = $res['classid'];
}
?>

<?php include $base .'header.php';?>
    <body>
        <div class="container">
            <form name="form1" method="post" action="editaction.php">
                <table border="0">
                    <td> 
                        <label>Name</label> <input type="text" name="name" value="<?php echo $name;?>">
                        <label>Age</label> <input type="text" name="age" value="<?php echo $age;?>">
                        <label>Email</label> <input type="text" name="email" value="<?php echo $email;?>">
                        <label>Gender</label> <input type="text" name="gender" value="<?php echo $gender;?>">
                        <label>Date Of Birth</label> <input type="text" name="dateofbirth" value="<?php echo $dateofbirth;?>">
                       
                        <label>Class ID</label> <input type="text" name="classid" value="<?php echo $classid;?>">

                       

                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">                    
                        <input type="submit" name="update" value="Update">
                    </td>
                </table>
            </form>
        </div>

    </body>
<?php include $base .'footer.php';?>
