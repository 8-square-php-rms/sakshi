<?php

$base = '../../include/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{    
   
    
    $subject_name = $crud->escape_string($_POST['subject_name']);
    $id = $crud->escape_string($_POST['id']);
    
    
    $msg = $validation->check_empty($_POST, array('subject_name', 'id',));
  
    
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    }  else {    
        //updating the table
        $result = $crud->execute("UPDATE subjects SET subject_name='$subject_name',id='$id' WHERE id=$id");
        
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>