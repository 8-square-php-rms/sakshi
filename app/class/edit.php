<?php
// including the database connection file
$base = '../../include/';
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//getting id from url
$id = $crud->escape_string($_GET['id']);
 
//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM classes WHERE id=$id");
 
foreach ($result as $res) {
    $class_name = $res['class_name'];
    $class_number = $res['class_number'];
    
}
?>

<?php include $base .'header.php';?>
    <body>
        <div class="container">
            <form name="form1" method="post" action="editaction.php">
                <table border="0">
                    <td> 
                        <label>Class Name</label> <input type="text" name="class_name" value="<?php echo $class_name;?>">
                        <label>Class Number</label> <input type="text" name="class_number" value="<?php echo $class_number;?>">
                        
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">                    
                        <input type="submit" name="update" value="Update">
                    </td>
                </table>
            </form>
        </div>

    </body>
<?php include $base .'footer.php';?>
