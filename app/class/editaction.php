<?php

$base = '../../include/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{   
    $id = $crud->escape_string($_POST['id']); 
    $class_name = $crud->escape_string($_POST['class_name']);
    $class_number = $crud->escape_string($_POST['class_number']);
    
    
    $msg = $validation->check_empty($_POST, array('class_name', 'class_number'));
    
    
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    }else {    
        //updating the table
        $result = $crud->execute("UPDATE classes SET class_name='$class_name',class_number='$class_number'  WHERE id=$id");
        
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>