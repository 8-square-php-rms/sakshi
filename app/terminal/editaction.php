<?php

$base = '../../include/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{    
    $id = $crud->escape_string($_POST['id']);
    
    $terminal_name = $crud->escape_string($_POST['terminal_name']);
    $studentid = $crud->escape_string($_POST['studentid']);
    $classid = $crud->escape_string($_POST['classid']);
    $subjectid = $crud->escape_string($_POST['subjectid']);
    $marksid = $crud->escape_string($_POST['marksid']);
    $subjectid = $crud->escape_string($_POST['subjectid']);
    $classid = $crud->escape_string($_POST['classid']);
    
    $msg = $validation->check_empty($_POST, array('terminal_name', 'studentid', 'classid','subjectid','marksid'));
    
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } else {    
        //updating the table
        $result = $crud->execute("UPDATE terminals SET terminal_name='$terminal_name',studentid='$studentid',classid='$classid',subjectid='$subjectid',marksid='$marksid' WHERE id=$id");
        
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>